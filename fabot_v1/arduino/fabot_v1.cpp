/*
 * Ultrasonic.cpp
 * A library for ultrasonic ranger
 *
 * Copyright (c) 2012 seeed technology inc.
 * Website    : www.seeed.cc
 * Author     : LG, FrankieChu
 * Create Time: Jan 17,2013
 * Change Log :
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "Arduino.h"
#include "fabot_v1.h"
#include "Ultrasonic.h"

static const int leftPWMPin = 3;
static const int leftCmd1Pin = 4;
static const int leftCmd2Pin = 7;
static const int rightPWMPin = 5;
static const int rightCmd1Pin = 8;
static const int rightCmd2Pin = 12;
static const int motorEnablePin = 13;
static const int leftUltrasonicPin = 2;
static const int rightUltrasonicPin = 6;

static const uint32_t tacc = 1000;

static void turnLeft(int angle) {
  // Left motor backward
  digitalWrite(leftCmd1Pin, HIGH);
  digitalWrite(leftCmd2Pin, LOW);

  // Right motor forward
  digitalWrite(rightCmd1Pin, HIGH);
  digitalWrite(rightCmd2Pin, LOW);

  // Set motors speed
  analogWrite(leftPWMPin, 100);
  analogWrite(rightPWMPin, 100);

  // Leave standby mode
  digitalWrite(motorEnablePin, HIGH);

  // Move duration
  delay( (int) angle * 27.8);

  // Put motor in standby mode
  digitalWrite(motorEnablePin, LOW);  
}

static void turnRight(int angle) {
  // Left motor forward
  digitalWrite(leftCmd1Pin, LOW);
  digitalWrite(leftCmd2Pin, HIGH);

  // Right motor backward
  digitalWrite(rightCmd1Pin, LOW);
  digitalWrite(rightCmd2Pin, HIGH);

  // Set motors speed
  analogWrite(leftPWMPin, 100);
  analogWrite(rightPWMPin, 100);

  // Leave standby mode
  digitalWrite(motorEnablePin, HIGH);

  // Move duration
  delay( (int) angle * 27.8);

  // Put motor in standby mode
  digitalWrite(motorEnablePin, LOW);  
}

fabot_v1::fabot_v1(void) : _leftUltrasonic(leftUltrasonicPin), _rightUltrasonic(rightUltrasonicPin) {
	_isMovingForward = false;
  _isEnabled = false;
  _metObstacle = false;

  pinMode(leftPWMPin, OUTPUT);
  pinMode(leftCmd1Pin, OUTPUT);
  pinMode(leftCmd2Pin, OUTPUT);
  pinMode(rightPWMPin, OUTPUT);
  pinMode(rightCmd1Pin, OUTPUT);
  pinMode(rightCmd2Pin, OUTPUT);
  pinMode(motorEnablePin, OUTPUT);

  // Put motor in standby mode
  digitalWrite(motorEnablePin, LOW);
}

void fabot_v1::startMovingForward(int speed) {

  _isMovingForward = true;
  
  // Left and Right motor forward
  digitalWrite(leftCmd1Pin, LOW);
  digitalWrite(leftCmd2Pin, HIGH);
  digitalWrite(rightCmd1Pin, HIGH);
  digitalWrite(rightCmd2Pin, LOW);

  // Set motors speed
  analogWrite(leftPWMPin, speed);
  analogWrite(rightPWMPin, speed);

  // Leave standby mode
  digitalWrite(motorEnablePin, HIGH);
}

void fabot_v1::stop(void) {
  digitalWrite(motorEnablePin, LOW);
  _isMovingForward = false;
}

void fabot_v1::turn(int angle, String side) {
  // Stop
  fabot_v1::stop();

  // Wait for the robot to stop
  delay(500);

  if ((side == "gauche") || (side == "left")) {
    turnLeft(angle);
  }
  else if ((side == "droite") || (side == "right")) {
    turnRight(angle);
  }
}

long fabot_v1::measureInCentimeters(String side) {
  if ((side == "gauche") || (side == "left")) {
    return _leftUltrasonic.MeasureInCentimeters();
  }
  if ((side == "droite") || (side == "right")) {
    return _rightUltrasonic.MeasureInCentimeters();
  }
}

void fabot_v1::manualyEnable(void) {
  while(fabot_v1::measureInCentimeters("left") > 20) {
  }
  while(fabot_v1::measureInCentimeters("left") < 30) {
  }
  while(fabot_v1::measureInCentimeters("right") > 20) {
  }
  while(fabot_v1::measureInCentimeters("right") < 30) {
  }
  while(fabot_v1::measureInCentimeters("left") > 20) {
  }
  while(fabot_v1::measureInCentimeters("left") < 30) {
  }
  _isEnabled = true;
}

void fabot_v1::moveForwardMilliseconds(int speed, uint32_t duration) {
  _metObstacle = false;
  uint32_t begin = millis();
  uint32_t x = 0;
  while (true) {
    if (min(fabot_v1::measureInCentimeters("left"), fabot_v1::measureInCentimeters("right")) < 25) {
      _metObstacle = true;
      fabot_v1::stop();
      break;
    }
    x = millis() - begin;
    if (x < min(tacc, duration/2)) {
      fabot_v1::startMovingForward( (int) speed * x / tacc);
    }
    else if (x < max(duration/2, duration - tacc)) {
      fabot_v1::startMovingForward(speed);
    }
    else if (x < duration) {
      fabot_v1::startMovingForward( (int) speed * (duration - x) / tacc);
    }
    else {
      fabot_v1::stop();
      break;
    }
  }
}

bool fabot_v1::metObstacle(void) {
  return _metObstacle;
}

bool fabot_v1::isMovingForward(void) {
  return _isMovingForward;
}

bool fabot_v1::isEnabled(void) {
  return _isEnabled;
}

void fabot_v1::enable(void) {
  _isEnabled = true;
}

void fabot_v1::disable(void) {
  _isEnabled = false;
}