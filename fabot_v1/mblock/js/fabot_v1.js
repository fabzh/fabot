﻿// demo.js

(function(ext) {
  var descriptor = {};
  var device = null;
  var _level = 0;
  var potentialDevices = [];
	var levels = {HIGH:1, LOW:0};
    
  function processData(bytes) { trace(bytes) }

  function tryNextDevice() {
    device = potentialDevices.shift();
    if (device) {
      device.open({ stopBits: 0, bitRate: 115200, ctsFlowControl: 0 }, deviceOpened);
    }
  }

  function deviceOpened(dev) {
    if (!dev) {
      tryNextDevice();
      return;
    }
    device.set_receive_handler('demo',function(data) {
      processData(data);
    });
  }

	ext.resetAll = function(){};
	
	ext.runArduino = function(){};
  
	ext.startMovingForward = function(speed) {};
	
	ext.MeasureInCentimeters = function(side) {return 0};

	ext.turn = function(angle, side) {};

	ext.manualyEnable = function() {};

  ext.stop = function(){};

	ext.moveForwardMilliseconds = function(speed, duration) {};

	ext.metObstacle = function() {return false};

	ext.isMovingForward = function() {return false};

	ext.isEnabled = function() {return false};

	ext.enable = function() {};

	ext.disable = function() {};

  ext._deviceConnected = function(dev) {
    potentialDevices.push(dev);
    if (!device) {
      tryNextDevice();
    }
  };

  ext._deviceRemoved = function(dev) {
      if(device != dev) return;
      device = null;
  };

  ext._shutdown = function() {
      if(device) device.close();
      device = null;
  };

  ext._getStatus = function() {
      if(!device) return {status: 1, msg: 'demo disconnected'};
      return {status: 2, msg: 'demo connected'};
  };

	ScratchExtensions.register('fabot_v1', descriptor, ext, {type: 'serial'});
})({});
